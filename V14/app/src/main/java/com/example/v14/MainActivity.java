package com.example.v14;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ConcreteRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<String> kontakti = new ArrayList<>();
        for(int i = 0; i < 100; i++){
            kontakti.add("Kontakt " + i);
        }

        RecyclerView rv = findViewById(R.id.recview);
        rv.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ConcreteRecyclerViewAdapter(this, kontakti);
        rv.setAdapter(adapter);
    }
}