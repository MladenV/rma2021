package com.example.v10viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//ViewModel mora da postoji nezavisno od fragmenata i activity-a(odvojen lifecycle),
// pa zato ne sme da ima referencu na njih.

public class ConcreteViewModel extends ViewModel {

    //Mutable - može se promeniti, za razliku od običnog LiveData
    private MutableLiveData<List<String>> data;
    private MutableLiveData<String> oneSelected = new MutableLiveData<>();

    //Vracamo LiveData koji je immutable, ko god koristi podatke, neće moći da ih menja
    //rad sa podacima treba realizovati ovde
    public LiveData<List<String>> getData(){
        //singleton pattern, radimo sa jednom instancom podataka
        //ukoliko ne postoji, dobavimo i instanciramo
        if(data == null){
            data = new MutableLiveData<>();
            fetchData(null);
        }
        return data;
    }

    //metoda koja dobavlja podatke sa eksternog izvora
    //u realnoj aplikaciji http zahtev/citanje iz baze
    public void fetchData(List<String> newData){
        if(newData == null){
            //mockujemo dobavljanje podataka
            newData = new ArrayList<>(Arrays.asList("Marko Markovic", "Petar Petrovic", "Milan Stefanovic"));
        }
        this.data.setValue(newData);
    }

    //komunikacija između fragmenata
    public void selectData(String item){
        this.oneSelected.setValue(item);
    }

    public LiveData<String> getSelected(){
        return this.oneSelected;
    }




}
