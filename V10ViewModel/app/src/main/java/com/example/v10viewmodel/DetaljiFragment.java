package com.example.v10viewmodel;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


public class DetaljiFragment extends Fragment {

    ConcreteViewModel viewModel;

    public DetaljiFragment() {
        // Required empty public constructor
    }


    public static DetaljiFragment newInstance() {
        DetaljiFragment fragment = new DetaljiFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity(), null).get(ConcreteViewModel.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detalji, container, false);
        LinearLayout layout = v.findViewById(R.id.detaljilayout);

        viewModel.getSelected().observe(getViewLifecycleOwner(), data -> {drawData(data, layout);});

        return v;
    }

    private void drawData(String data, ViewGroup container){
        container.removeAllViews();
        TextView t = new TextView(requireContext());
        t.setText(data);
        container.addView(t);
    }
}