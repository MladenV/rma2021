package com.example.v10viewmodel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.Button;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    ConcreteViewModel vm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.kontaktifragmentholder, KontaktiFragment.newInstance())
                .add(R.id.detaljifragmentholder, DetaljiFragment.newInstance())
                .commit();

        //ovaj deo je za demonstraciju samo, idealno nam main activity ne radi ništa, osim navigacije
        //izmenićemo podatke u viewModelu, da bismo pokazali da se prikaz u fragmentima odmah ažurira
        vm = new ViewModelProvider(this, new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                try{
                    return modelClass.newInstance();
                }catch (Exception e){
                    return null;
                }
            }
        }).get(ConcreteViewModel.class);
        ((Button)findViewById(R.id.dugme)).setOnClickListener(view -> {
            vm.fetchData(Arrays.asList("Darko Markovic", "Milan Gajic"));
        });
    }
}