package com.example.v10viewmodel;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;


public class KontaktiFragment extends Fragment {
    ConcreteViewModel viewModel;

    public KontaktiFragment() {
        // Required empty public constructor
    }


    public static KontaktiFragment newInstance() {
        KontaktiFragment fragment = new KontaktiFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity(), new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                try {
                    return modelClass.newInstance();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (java.lang.InstantiationException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }).get(ConcreteViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_kontakti, container, false);
        LinearLayout layout = v.findViewById(R.id.kontaktilayout);
        //kad god se ažurira lista kontakata, treba ponovo iscrtati view
        viewModel.getData().observe(getViewLifecycleOwner(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> strings) {
                drawData(strings, layout);
            }
        });

        //alternativno
        //viewModel.getData().observe(getViewLifecycleOwner(), data -> { drawData(data, layout);});

        return v;
    }

    private void drawData(List<String> data, ViewGroup container){
        container.removeAllViews(); //pobrisemo ako postoje vec
        for(String s : data){
            TextView t = new TextView(requireActivity());
            t.setText(s);
            t.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewModel.selectData(((TextView)view).getText().toString());
                }
            });
            container.addView(t);
        }
    }
}