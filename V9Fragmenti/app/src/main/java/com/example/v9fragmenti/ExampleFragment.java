package com.example.v9fragmenti;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ExampleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExampleFragment extends Fragment {

    //parametri koji se koriste pri kreiranju fragmenta
    //nisu neophodni, definišu se samo ako je potrebno za fragment
    private static final String NAZIV_DUGMETA = "param1";
    private static final String TEKST_TOASTA = "param2";

    private String mParam1;
    private String mParam2;

    public ExampleFragment() {
        // Required empty public constructor
    }

    //factory metoda za instanciranje fragmenta
    public static ExampleFragment newInstance(String param1, String param2) {
        ExampleFragment fragment = new ExampleFragment();
        Bundle args = new Bundle();

        args.putString(NAZIV_DUGMETA, param1);
        args.putString(TEKST_TOASTA, param2);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //preuzimanje vrednosti iz factory metode i dodavanje u atribute instance fragmenta
        if (getArguments() != null) {
            mParam1 = getArguments().getString(NAZIV_DUGMETA);
            mParam2 = getArguments().getString(TEKST_TOASTA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_example, container, false);
        ((Button)v.findViewById(R.id.fragmentdugme)).setText(mParam1);
        ((Button)v.findViewById(R.id.fragmentdugme)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), mParam2, Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }
}