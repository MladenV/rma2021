package com.example.v9fragmenti;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private boolean toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        toggle = false;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ExampleFragment ef = ExampleFragment.newInstance("Dugme", "Primer teksta");
        ft.add(R.id.fragmentframe, ef);
        ft.commit();

        findViewById(R.id.swap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swap();
            }
        });

    }

    private void swap(){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if(toggle){

            ExampleFragment ef = ExampleFragment.newInstance("Dugme", "Primer teksta");
            ft.replace(R.id.fragmentframe, ef, "1");
            toggle = false;
        }else{
            ExampleFragment2 ef = ExampleFragment2.newInstance("", "");
            ft.replace(R.id.fragmentframe, ef, "2");
            toggle = true;
        }

        ft.commit();
    }

}