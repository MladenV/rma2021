package com.example.v9fragmenti2;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PorukeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PorukeFragment extends Fragment {

    private ArrayList<String> poruke;

    private void setPoruke(ArrayList<String> poruke){
        this.poruke = poruke;
    }

    public PorukeFragment() {
        // Required empty public constructor
    }

    public static PorukeFragment newInstance(ArrayList<String> poruke) {
        PorukeFragment fragment = new PorukeFragment();
        fragment.setPoruke(poruke);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_poruke, container, false);
        LinearLayout layout = v.findViewById(R.id.porukelayout);

        for(String poruka : poruke){
            TextView t = new TextView(getContext());
            t.setText(poruka);
            layout.addView(t);
        }
        return v;
    }
}