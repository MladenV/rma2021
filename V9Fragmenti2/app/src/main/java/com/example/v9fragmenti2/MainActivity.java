package com.example.v9fragmenti2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements CustomInteraction{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initFragment(generateData());
    }

    public void initFragment(ArrayList<Kontakt> k){

        KontaktiFragment kf = KontaktiFragment.newInstance(k, this);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ft.add(R.id.fragmentcontainer, kf, "kontakti");
        ft.commit();

    }

    public void swapFragment(int index){
        PorukeFragment pf = PorukeFragment.newInstance( generateData().get(index).getPoruke());
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ft.replace(R.id.fragmentcontainer, pf, "poruke");
        ft.addToBackStack("poruke");
        ft.commit();
    }

    private ArrayList<Kontakt> generateData(){
        ArrayList<Kontakt> kontakti = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            Kontakt k = new Kontakt();
            k.setIme("ime " + i);
            k.setPrezime("prezime " + i);
            ArrayList<String> poruke = new ArrayList<>();
            for(int j = 0; j < 5; j++){
                poruke.add("Sadrzaj poruke korisnika " + i + ". Redni broj poruke je " +j + ".");
            }
            k.setPoruke(poruke);
            kontakti.add(k);
        }
        return kontakti;
    }

    @Override
    public void selectedItem(int index) {
        swapFragment(index);
    }
}