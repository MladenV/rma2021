package com.example.v9fragmenti2;

import java.io.Serializable;
import java.util.ArrayList;

public class Kontakt implements Serializable {

    private String ime;
    private String prezime;
    private ArrayList<String> poruke;

    public Kontakt(){

    }

    public String getIme(){
        return ime;
    }

    public void setIme(String ime){
        this.ime = ime;
    }

    public String getPrezime(){
        return prezime;
    }

    public void setPrezime(String prezime){
        this.prezime = prezime;
    }

    public ArrayList<String> getPoruke(){
        return poruke;
    }

    public void setPoruke(ArrayList<String> poruke){
        this.poruke = poruke;
    }



}
