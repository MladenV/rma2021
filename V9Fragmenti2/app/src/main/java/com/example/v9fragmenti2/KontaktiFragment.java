package com.example.v9fragmenti2;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class KontaktiFragment extends Fragment{


    private ArrayList<Kontakt> kontakti;
    private CustomInteraction parent;

    private void setParent(CustomInteraction parent){
        this.parent = parent;
    }

    private void setKontakti(ArrayList<Kontakt> kontakti){
        this.kontakti = kontakti;
    }

    public KontaktiFragment() {
        // Required empty public constructor
    }



    public static KontaktiFragment newInstance(ArrayList<Kontakt> param1, CustomInteraction parent) {
        KontaktiFragment fragment = new KontaktiFragment();
        fragment.setParent(parent);
        fragment.setKontakti(param1);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("Pozvana onCreate metoda");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_kontakti, container, false);
        LinearLayout layout = v.findViewById(R.id.kontaktilayout);
        for(Kontakt k : kontakti){
            TextView t = new TextView(getContext());
            t.setText(k.getIme() + " " + k.getPrezime());
            t.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    parent.selectedItem(kontakti.indexOf(k));
                }
            });
            layout.addView(t);
        }
        System.out.println("Pozvana onCreateView metoda");
        return v;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        System.out.println("Pozvana onAttach metoda");
    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("Pozvana onPause metoda");
    }
}